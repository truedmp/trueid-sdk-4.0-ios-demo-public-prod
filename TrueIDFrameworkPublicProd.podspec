#
#  Be sure to run `pod spec lint TrueIDFrameworkPublicProd.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see http://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |s|

  # ―――  Spec Metadata  ―――――――――――――――――――――――――――――――――――――――――――――――――――――――――― #
  #
  #  These will help people to find your library, and whilst it
  #  can feel like a chore to fill in it's definitely to your advantage. The
  #  summary should be tweet-length, and the description more in depth.
  #

s.name             = "TrueIDFrameworkPublicProd"
s.version          = '1.0.1'
s.summary          = 'True SDK Login for swift 4.2'

s.description      = <<-DESC
TODO: Add long description of the pod here.
version 0.1.8
-- Added
- Print log when application using SDKV.4 that operating.
-- Fixed Bug
- Application is log in after Application using SDK V.3 that
update to SDK v.4
- TrueId Application is loged in after Application using SDK V.3 that
update to SDK v.4
- Applicatin hide progress bar after forget password success for Ipad Device.=
DESC

s.homepage         = 'https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public-prod'
# s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
s.license          = { :type => 'MIT', :file => 'LICENSE' }
s.author           = { 'willshere' => 'kowit_nan@truecorp.cp.th' }
s.source           = { :git => 'https://bitbucket.org/truedmp/trueid-sdk-4.0-ios-demo-public-prod.git', :tag => s.version.to_s }
# s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

s.ios.deployment_target = '8.0'

s.ios.vendored_frameworks = "TrueIDFrameworkPublicProd/Framework/TrueIDFramework.framework"
#s.source_files = 'TrueIDFrameworkPublicProd/Classes/**/*'

# s.resource_bundles = {
#   'TrueIDFrameworkPublicProd' => ['TrueIDFrameworkPublicProd/Assets/*.png']
# }

# s.public_header_files = 'Pod/Classes/**/*.h'
# s.frameworks = 'UIKit', 'MapKit'
# s.dependency 'AFNetworking', '~> 2.3'
end

